<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('register');

    }
    public function hasil(Request $request)
    {
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        return view('welcome', compact('first_name', 'last_name'));
    }
}
