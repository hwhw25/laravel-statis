<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First Name : </label><br><br>
        <input type="text" name="first_name" required> <br></br>
        <label>Last Name : </label><br><br>
        <input type="text" name="last_name" required><br></br>
        <label>Gender :</label><br><br>
        <input type ="radio" name="jk" value="1">Male<br>
        <input type ="radio" name="jk" value="2">Female<br><br>
        <label>Nationality</label><br><br>
        <select name="Nationality"><br>
        <option value ="Indonesia">Indonesia</option>
        <option value ="America">America</option>
        <option value ="Inggris">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type ="checkbox">Bahasa Indonesia<br>
        <input type ="checkbox">English<br>
        <input type ="checkbox">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br>
        <input type="submit" value="Sign Up"></input>
        </form>
</body>
</html>